///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Andee Gary  <andeeg@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @11_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "fish.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "animalfactory.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	//Creating animal array and filling them
   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   for (int i = 0; i < 25 ; i++){
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }
   cout << endl;
   cout << "Array of Animals" << endl;
   cout << "   Is it empty:   "  << boolalpha << animalArray.empty()  << endl;
   cout << "   Number of elements:  " << animalArray.size() << endl;
   cout << "   Max size:   " << animalArray.max_size() << endl;
	
   for ( Animal* animal : animalArray ){
     //When animal is Null we want to continue past it so no segmentation fault
      if( animal == NULL){
         continue;
      }
      cout << animal->speak() << endl;
   }
   //Delete animals and start destructors
   for ( Animal* animal : animalArray) {
      delete animal;
   }
         

   //Create an AnimalList
   list<Animal*> animalList;
   for (int i = 0 ; i < 25 ; i++) {
      animalList.push_front( AnimalFactory::getRandomAnimal());
   }
   cout << endl;
   cout << "List of Animals" << endl;
   cout << "   Is it empty:   " << boolalpha << animalList.empty() << endl;
   cout << "   Number of elements:  " << animalList.size() << endl;
   cout << "   Max size:   " << animalList.max_size() << endl;
   
   for ( Animal* animal : animalList ) {
      cout << animal->speak() << endl;
   }
   for ( Animal* animal : animalList ) {
      delete animal;
   }
   


   return 0;
}
