///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.cpp
/// @version 1.0
///
/// Exports data about all aku fish
///
/// @author @Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @18_Feb_2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "aku.hpp"

using namespace std;

namespace animalfarm {

Aku::Aku( float Weight, enum Color newColor, enum Gender newGender ){
      gender = newGender;
      species = "Katsuwonus pelamis";
      gender = newGender;
      scaleColor = newColor;
      favoriteTemp = 75;
      weight = Weight;
}

void Aku::printInfo(){
   cout << "Aku" << endl;
   cout << "   Weight = [" << weight << "]" << endl;
   Fish::printInfo();
}
}
