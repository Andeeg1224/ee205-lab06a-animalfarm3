///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author @Andee Gary <andeeg@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   @4_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <random>


#include "animal.hpp"

using namespace std;

namespace animalfarm {

static random_device rD;
static mt19937 RNG( rD() );
static bernoulli_distribution boolRNG( 0.5 );

Animal::Animal(){
   cout << "." ;
}

Animal::~Animal(){
   cout << "x" ;
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, Really, Unknown");
};
	

string Animal::colorName (enum Color color) {
	switch (color) {
      case BLACK:    return string("Black");  break;
      case WHITE:    return string("White");  break;
      case RED:      return string("Red");    break;
      case SILVER:   return string("Silver"); break;
      case YELLOW:   return string("Yellow"); break;
      case BROWN:    return string("Brown");  break;
   }
   return string("Unknown");
};
	
   const Gender Animal::getRandomGender(){
      if( boolRNG( RNG ))
            return MALE; //Return the corresponding bool for each gender
      else
            return FEMALE;
   }

   const enum Color Animal::getRandomColor(){
      uniform_int_distribution<> colorRNG(BLACK, BROWN);
         int c = colorRNG( RNG );

         switch(c){
         case 0: return BLACK;   break;
         case 1: return WHITE;   break;
         case 2: return RED;     break;
         case 3: return SILVER;  break;
         case 4: return YELLOW;  break;
         case 5: return BROWN;   break;
         }
      return BLACK; //Color to be returned as default, should not be used
      }
   
   const bool Animal::getRandomBool(){
      return boolRNG( RNG ); //return a random bool
   }
   
   const float Animal::getRandomWeight(const float from, const float to){
      uniform_real_distribution<> floatRNG(from, to);
      float w = floatRNG( RNG );
      return w; //Returns w, which is float weight
   }
   
   const string Animal::getRandomName(){
      uniform_int_distribution<> lengthRNG(4, 15);
      int length = lengthRNG( RNG );
      
      uniform_int_distribution<> uppercaseRNG('A', 'Z');
      uniform_int_distribution<> lowercaseRNG('a', 'z');
      
      char Randnamearr[length];
      
      Randnamearr[0] = uppercaseRNG( RNG );
      for(int i = 1 ; i <= length ; i++){
        Randnamearr[i] = lowercaseRNG( RNG );
      }
      Randnamearr[length] = 0; //Set last bit of array = 0
      return Randnamearr; //Return rand name array 
   }


} // namespace animalfarm
