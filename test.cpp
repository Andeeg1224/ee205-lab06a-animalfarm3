///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file test.cpp
/// @version 1.0
///
///Test file for Animal Farm 3
///
/// @author @Andee Gary  <andeeg@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   @04_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "animal.hpp"
#include "mammal.hpp"
#include "bird.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "fish.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "animalfactory.hpp"

using namespace std;
using namespace animalfarm;

int main(){
   //Dog testdog( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender());
   //testdog.printInfo();
   
   //Aku testaku( Animal::getRandomWeight(18.0,22.0) , SILVER , Animal::getRandomGender());
   //testaku.printInfo();
   
   //Nunu testnunu( Animal::getRandomBool(), RED, Animal::getRandomGender());
   //testnunu.printInfo();
   cout << "Gender " << Animal::getRandomGender() << endl;
   cout << "Color " << Animal::getRandomColor() << endl;
   cout << "Bool  " << boolalpha << Animal::getRandomBool() << endl;
   cout << "Weight   " << Animal::getRandomWeight(18.0, 22.0) << endl;
   cout << "Name  " << Animal::getRandomName() << endl;

   for (int i = 0; i < 25; i++ ) {
      Animal* a = AnimalFactory::getRandomAnimal();
      cout << a->speak() << endl;
   }
}
